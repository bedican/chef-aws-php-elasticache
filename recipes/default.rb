#
# Cookbook Name:: aws-php-elasticache
# Recipe:: default
#

include_recipe "php"

tarball = "#{Chef::Config[:file_cache_path]}/aws-php-elasticache";

remote_file tarball do
    source "https://elasticache-downloads.s3.amazonaws.com/ClusterClient/PHP-#{node['aws_php_elasticache']['php_version']}/latest-#{node['aws_php_elasticache']['arch']}bit"
    action :create
end

execute "pecl install #{tarball}" do
    action :run
end

execute 'echo "extension=$(pear config-get ext_dir)/amazon-elasticache-cluster-client.so" > /etc/php5/mods-available/memcached.ini' do
    action :run
end

execute "php5enmod memcached" do
    action :run
end
